module.exports = {
  siteUrl: process.env.FRONT_PAGE_URL,
  generateRobotsTxt: true,
};
